<?php
namespace App\Services;


use App\Repositories\Slider\SliderRepositories;
use App\Slider;

class SliderService
{
    protected $slider_repositories;
    protected $slider;

    /**
     * SliderService constructor.
     *
     * @param SliderRepositories $slider_repositories
     * @param Slider $slider
     */

    public function __construct(SliderRepositories $slider_repositories , Slider $slider)
    {
        $this->slider_repositories = $slider_repositories;
        $this->slider = $slider;
    }

    /**
     * @param $request
     *
     * @return mixed
     */

    public function getAllSliderAndSearch($request)
    {
        $search = '';
        if($request->search)
        {
            $search = $request->search;
        }
        return $this->slider_repositories->getAllSlider($search);
    }

    public function createSide($request , $thumbnailSlide)
    {
       return $this->slider::create([
            'title_slide' => $request->title_slide,
            'thumbnail_slide' => url('uploads/'.$thumbnailSlide),
            'user_id' => $request->user_id,
            'status' => $request->status
        ]);
    }
}
