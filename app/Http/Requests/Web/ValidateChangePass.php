<?php

namespace App\Http\Requests\Web;

use Illuminate\Foundation\Http\FormRequest;

class ValidateChangePass extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password_old'          => 'required',
            'password'              => 'required|min:8|confirmed',
            'password_confirmation' => 'required|same:password'
            //
        ];
    }

    public function attributes()
    {
        return [
            'password_old'          => 'Mật khẩu cũ',
            'password'              => 'Mật khẩu ',
            'password_confirmation' => 'Nhập lại mật khẩu',
        ];
    }

    public function messages()
    {
        return [
            'required'  => ':attribute không được để trống',
            'password.min'     => ':attribute lớn hơn 8 kí tự',
            'password.confirmed' => ':attribute không trùng khớp',
            //
        ];
    }
}
