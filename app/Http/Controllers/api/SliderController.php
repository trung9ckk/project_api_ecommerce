<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;

use App\Repositories\Slider\SliderRepositories;
use App\Services\SliderService;
use App\Slider;
use Illuminate\Http\Request;

class SliderController extends Controller
{

    protected $slider_repositories;
    protected $slider_service;

    /**
     * SliderController constructor.
     *
     * @param SliderRepositories $slider_repositories
     * @param SliderService $slider_service
     */

    public function __construct(SliderRepositories $slider_repositories , SliderService $slider_service)
    {
        $this->slider_repositories = $slider_repositories;
        $this->slider_service = $slider_service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $listSlide = $this->slider_service->getAllSliderAndSearch($request);
        return \App\Http\Resources\Slider::collection($listSlide);
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * @param Request $request
     */

    public function store(Request $request)
    {
        $thumbnailSlide = uploadImg($request , public_path() . '/uploads/');
         $this->slider_service->createSide($request , $thumbnailSlide);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $sliderItem = $this->slider_repositories->getItemSlider($id);
        return new \App\Http\Resources\Slider($sliderItem);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {
        //
    }
}
