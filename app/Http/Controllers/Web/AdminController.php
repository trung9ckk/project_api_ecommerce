<?php

namespace App\Http\Controllers\Web;

use App\Admin;
use App\Http\Controllers\Controller;
use App\Http\Requests\Web\ValidateChangePass;
use App\Services\AuthService;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use PhpParser\Lexer\TokenEmulator\ReadonlyTokenEmulator;

class AdminController extends Controller
{
    protected $auth_service;
    protected $admin;

    /**
     * AdminController constructor.
     *
     * @param AuthService $auth_service
     * @param Admin $admin
     */

    public function __construct(AuthService $auth_service , Admin $admin)
    {
        $this->auth_service = $auth_service;
        $this->admin = $admin;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list_user = Admin::paginate();

        return view('admin.users.list', compact('list_user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */

    public function show($id)
    {
        $user = Admin::find($id);

        return view('admin.users.infoAdmin', compact('user'));
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function profileAuth(Request $request)
    {
        $user = Admin::find(Auth::id());

        return view('admin.users.infoAdmin', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $isUpdate = Admin::where('id', $id)->update($request->except('_method', '_token'));
        if ($isUpdate) {
            return redirect()->route('user.index')->with('success', 'Bạn đã cập nhật thông tin tài khoản thành công');
        } else {
            return redirect()->route('user.index')->with('error', 'Bạn đã cập nhật thông tin tài khoản thất bại');
        }
    }

    /**
     * @param Request $request
     *
     * @return string
     */

    public function changePassword(Request $request)
    {
        $user = Admin::find(Auth::id());

        return view('admin.users.change_pass', compact('user'));
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */

    public function updateChangePass(ValidateChangePass $request)
    {
        $isChangePass = $this->auth_service->changePassword($request , 'admin');
        if ($isChangePass) {
            return redirect()->route('user.change_pass')->with('success', 'Bạn đã đổi mật khẩu thành công');
        } else {
            return redirect()->route('user.change_pass')->with('error', 'Mật khẩu cũ không trùng khớp');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
