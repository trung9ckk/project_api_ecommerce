<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    /**
     * @var string[]
     */

    protected $fillable =[
        'title_slide',
        'thumbnail_slide',
        'user_id',
        'status'
    ];

    /**
     * @var string
     */

    protected $table = 'sliders';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */

    public function user()
    {
        return $this->belongsTo('App\User' , 'user_id');
    }

}
