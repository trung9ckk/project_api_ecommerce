<?php
namespace App\Repositories\Slider;

use App\Slider;

class SliderRepositories implements SliderRepositoryInterface
{
    protected $slider;

    /**
     * SliderRepositories constructor.
     *
     * @param Slider $slider
     */

    public function __construct(Slider $slider)
    {
        $this->slider = $slider;
    }

    /**
     * @param $search
     *
     * @return mixed
     */

    public function getAllSlider($search)
    {
        return $this->slider::where([
            ['title_slide' ,'LIKE' , "%{$search}"],
            ['status' , 'public'],
        ])->get();
    }

    /**
     * @param $id
     *
     * @return mixed
     */

    public function getItemSlider($id)
    {
        return $this->slider::find($id);
    }
}
