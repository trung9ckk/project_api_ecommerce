<?php

namespace App\Repositories\Slider;

interface SliderRepositoryInterface
{
    public function getAllSlider($search);

    public function getItemSlider($id);
}
