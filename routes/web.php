<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/' ,'HomeController@index');


Auth::routes();
Route::middleware('auth')->group(function () {
//    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/' , 'Web\DashboardController@show')->name('admin');
    Route::get('/admin' , 'Web\DashboardController@show')->name('admin.dashboard');

//    admin user
    Route::get('admin/user/change_pass', 'Web\AdminController@changePassword')->name('user.change_pass');
    Route::post('admin/user/update_change_pass', 'Web\AdminController@updateChangePass')->name('user.update_change_pass');
    Route::get('admin/user/profile', 'Web\AdminController@profileAuth')->name('user.profile');
    Route::resource('admin/user', 'Web\AdminController');
});
