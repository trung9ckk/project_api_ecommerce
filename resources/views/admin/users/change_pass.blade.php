@extends('layouts.admin', ['menu_type' => 'menu_profile' , 'key' => 'profile_change_pass'])

@section('content')
    <div id="content" class="container-fluid">
        <div class="card">
            <div class="card-header font-weight-bold">
                Đổi mật khẩu
            </div>
            <div class="card-body col-md-4">
                <form action="{{ route('user.update_change_pass') }}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="username">Mât khẩu cũ</label>
                        @error('password_old')
                            <div class="text text-danger">{{ $message }}</div>
                        @enderror
                        <input  class="form-control" type="password" name="password_old" id="username" >
                    </div>
                    <div class="form-group">
                        <label for="name">Mật khẩu mới</label>
                        @error('password')
                        <div class="text text-danger">{{ $message }}</div>
                        @enderror
                        <input class="form-control" type="password" name="password" id="name">
                    </div>
                    <div class="form-group">
                        <label for="password_confirmation">Nhập lại mật khẩu</label>
                        @error('password_confirmation')
                        <div class="text text-danger">{{ $message }}</div>
                        @enderror
                        <input class="form-control" type="password" name="password_confirmation" id="password_confirmation">
                    </div>
                    <button type="submit" class="btn btn-primary">Cập nhật</button>
                </form>
            </div>
        </div>
    </div>
@endsection
