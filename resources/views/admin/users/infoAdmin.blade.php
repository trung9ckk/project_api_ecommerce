@extends('layouts.admin', ['menu_type' => 'menu_profile' , 'key' => 'profile_info'])

@section('content')
    <div id="content" class="container-fluid">
        <div class="card">
            <div class="card-header font-weight-bold">
                Chi tiết tài khoản
            </div>
            <div class="card-body col-md-4">
                <form action="{{ route('user.update' , $user->id) }}" method="POST">
                    @method('PATCH')
                    @csrf
                    <div class="form-group">
                        <label for="username">Tên tài khoản</label>
                        <input disabled class="form-control" type="text" name="username" id="username" value="admin">
                    </div>
                    <div class="form-group">
                        <label for="name">Họ và tên</label>
                        <input class="form-control" type="text" name="name" id="name" value="{{ $user->name }}">
                    </div>
                    <div class="form-group">
                        <label for="email">Địa chỉ email (đăng nhập)</label>
                        <input class="form-control" type="text" name="email" id="email" value="{{ $user->email }}">
                    </div>
                    <button type="submit" class="btn btn-primary">Cập nhật</button>
                </form>
            </div>
        </div>
    </div>
@endsection
