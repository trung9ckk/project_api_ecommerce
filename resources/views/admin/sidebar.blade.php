@php

    @endphp

{{--sidebar menu--}}
@if($arrayStatus['menu_type'] == 'menu_sidebar')
    <ul id="sidebar-menu">
        @foreach(\Illuminate\Support\Facades\Config::get('sidebars.menu_sidebar') as $menu_sidebar)
            <li class="nav-link {{ $menu_sidebar['key'] == $arrayStatus['key'] ? 'active' : ''}}">
                <a href="{{ !empty($menu_sidebar['route']) ? route($menu_sidebar['route'] , !empty($arrayStatus['id']) ? $arrayStatus['id'] :'' ) : '#' }}">
                    <div class="nav-link-icon d-inline-flex">
                        <i class="far {{ !empty($menu_sidebar['icon']) ? $menu_sidebar['icon'] : '' }}"></i>
                    </div>
                    {{ !empty($menu_sidebar['name']) ? $menu_sidebar['name'] : '' }}
                </a>
                <i class="arrow fas fa-angle-right"></i>
                @if(isset($menu_sidebar['children']))
                    <ul class="sub-menu">
                        @foreach($menu_sidebar['children'] as $menu_sidebar_child)
                            <li>
                                <a href="{{ !empty($menu_sidebar_child['route']) ? route($menu_sidebar_child['route'] , !empty($arrayStatus['id']) ? $arrayStatus['id'] :'' ) : '#' }}">
                                    {{ !empty($menu_sidebar_child['name']) ? $menu_sidebar_child['name'] :'' }}
                                </a></li>
                        @endforeach
                    </ul>
                @endif
            </li>
        @endforeach
        {{--        <li class="nav-link">--}}
        {{--            <a href="?view=list-post">--}}
        {{--                <div class="nav-link-icon d-inline-flex">--}}
        {{--                    <i class="far fa-folder"></i>--}}
        {{--                </div>--}}
        {{--                Trang--}}
        {{--            </a>--}}
        {{--            <i class="arrow fas fa-angle-right"></i>--}}

        {{--            <ul class="sub-menu">--}}
        {{--                <li><a href="?view=add-post">Thêm mới</a></li>--}}
        {{--                <li><a href="?view=list-post">Danh sách</a></li>--}}
        {{--            </ul>--}}
        {{--        </li>--}}
        {{--        <li class="nav-link">--}}
        {{--            <a href="?view=list-post">--}}
        {{--                <div class="nav-link-icon d-inline-flex">--}}
        {{--                    <i class="far fa-folder"></i>--}}
        {{--                </div>--}}
        {{--                Bài viết--}}
        {{--            </a>--}}
        {{--            <i class="arrow fas fa-angle-right"></i>--}}
        {{--            <ul class="sub-menu">--}}
        {{--                <li><a href="?view=add-post">Thêm mới</a></li>--}}
        {{--                <li><a href="?view=list-post">Danh sách</a></li>--}}
        {{--                <li><a href="?view=cat">Danh mục</a></li>--}}
        {{--            </ul>--}}
        {{--        </li>--}}
        {{--        <li class="nav-link">--}}
        {{--            <a href="?view=list-product">--}}
        {{--                <div class="nav-link-icon d-inline-flex">--}}
        {{--                    <i class="far fa-folder"></i>--}}
        {{--                </div>--}}
        {{--                Sản phẩm--}}
        {{--            </a>--}}
        {{--            <i class="arrow fas fa-angle-down"></i>--}}
        {{--            <ul class="sub-menu">--}}
        {{--                <li><a href="?view=add-product">Thêm mới</a></li>--}}
        {{--                <li><a href="?view=list-product">Danh sách</a></li>--}}
        {{--                <li><a href="?view=cat-product">Danh mục</a></li>--}}
        {{--            </ul>--}}
        {{--        </li>--}}
        {{--        <li class="nav-link">--}}
        {{--            <a href="?view=list-order">--}}
        {{--                <div class="nav-link-icon d-inline-flex">--}}
        {{--                    <i class="far fa-folder"></i>--}}
        {{--                </div>--}}
        {{--                Bán hàng--}}
        {{--            </a>--}}
        {{--            <i class="arrow fas fa-angle-right"></i>--}}
        {{--            <ul class="sub-menu">--}}
        {{--                <li><a href="?view=list-order">Đơn hàng</a></li>--}}
        {{--            </ul>--}}
        {{--        </li>--}}
        {{--        <li class="nav-link {{ $active == 'admin' ? 'active' : ''  }}">--}}
        {{--            <a href={{ route('user.index')  }}>--}}
        {{--                <div class="nav-link-icon d-inline-flex">--}}
        {{--                    <i class="far fa-folder"></i>--}}
        {{--                </div>--}}
        {{--                Users--}}
        {{--            </a>--}}
        {{--            <i class="arrow fas fa-angle-right"></i>--}}

        {{--            <ul class="sub-menu">--}}
        {{--                <li><a href="?view=add-user">Thêm mới</a></li>--}}
        {{--                <li><a href={{ route('user.index') }}>Danh sách</a></li>--}}
        {{--            </ul>--}}
        {{--        </li>--}}
    </ul>

    {{--sidebar menu info--}}
@elseif($arrayStatus['menu_type'] == 'menu_profile')
    <ul id="sidebar-menu">
        @foreach(\Illuminate\Support\Facades\Config::get('sidebars.menu_profile') as $menu_profile )
            <li class="nav-link {{ $menu_profile['key'] == $arrayStatus['key'] ? 'active' : ''}}">
                <a href="{{ !empty($menu_profile['route']) ? route($menu_profile['route'] , !empty($arrayStatus['id']) ? $arrayStatus['id'] :'' ) : '#'  }}"
                   @if($menu_profile['route'] == 'logout')
                   onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();"
                    @endif
                >
                    <div class="nav-link-icon d-inline-flex">
                        <i class="fas {{ !empty($menu_profile['icon']) ? $menu_profile['icon'] :'' }}"></i>
                    </div>
                    {{ !empty($menu_profile['name']) ? $menu_profile['name'] :'' }}
                </a>
                <i class="arrow fas fa-angle-right"></i>
            </li>
        @endforeach
    </ul>
@endif
<form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
    @csrf
</form>



