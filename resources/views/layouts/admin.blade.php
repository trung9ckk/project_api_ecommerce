<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/css/all.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/css/solid.min.css">
    <link rel="stylesheet" href={{ asset('css/app.css') }}>
    <title>Admintrator</title>
</head>

<body>
<div id="warpper" class="nav-fixed">
    <nav class="topnav shadow navbar-light bg-white d-flex">
        <div class="navbar-brand"><a href="?">SHOP ADMIN</a></div>
        <div class="nav-right ">
            <div class="btn-group mr-auto">
                <button type="button" class="btn dropdown" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false">
                    <i class="plus-icon fas fa-plus-circle"></i>
                </button>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="?view=add-post">Thêm bài viết</a>
                    <a class="dropdown-item" href="?view=add-product">Thêm sản phẩm</a>
                    <a class="dropdown-item" href="?view=list-order">Thêm đơn hàng</a>
                </div>
            </div>
            <div class="btn-group">
                <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false">
                    {{ \Illuminate\Support\Facades\Auth::user()->name  }}
                </button>
                    <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="{{ route('user.profile') }}">Tài
                        khoản</a>
                    <a class="dropdown-item" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
                </div>
            </div>
        </div>
    </nav>
    <!-- end nav  -->
    <div id="page-body" class="d-flex">
        <div id="sidebar" class="bg-white">
            @php
                $arrayStatus = [];
                    if(isset($user_id))
                    {
                        $arrayStatus['id'] = $user_id;
                    }
                    if(isset($menu_type))
                    {
                            $arrayStatus['menu_type'] = $menu_type;
                    }
                    if(isset($key))
                    {
                            $arrayStatus['key'] = $key;
                    }
            @endphp
            @include('admin.sidebar' , compact('arrayStatus'))
        </div>
        <div id="wp-content">
            @if(session('success'))
                <div class="alert alert-success mt-4">
                    {{ session('success') }}
                </div>
            @endif
            @if(session('error'))
                <div class="alert alert-danger mt-4">
                    {{ session('error') }}
                </div>
            @endif
            @yield('content')
        </div>
    </div>


</div>

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src={{ asset('js/js.js') }}></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
</body>

</html>
