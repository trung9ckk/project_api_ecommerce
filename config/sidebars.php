<?php
return [
    'menu_profile' => [
        [
            'name'  => 'Thông tin tài khoản',
            'icon'  => 'fa-user',
            'key'   => 'profile_info',
            'route' => 'user.profile',
        ],
        [
            'name'  => 'Đổi mật khẩu',
            'icon'  => 'fa-key',
            'key'   => 'profile_change_pass',
            'route' => 'user.change_pass',
        ],
        [
            'name'  => 'Đăng xuất',
            'icon'  => 'fa-sign-out-alt',
            'key'   => 'logout',
            'route' => 'logout',
        ],
    ],

//        menu_sidebar
    'menu_sidebar' => [
        [
            'name'  => 'Dashboard',
            'icon'  => 'fa-folder',
            'key'   => 'dashboard',
            'route' => 'admin.dashboard',
        ],
        [
            'name'     => 'Users',
            'icon'     => 'fa-folder',
            'key'      => 'list_user',
            'route'    => 'user.index',
            'children' => [
                [
                    'name'  => 'Danh sách',
                    'icon'  => '',
                    'key'   => 'list_user',
                    'route' => 'user.index',
                ],
                [
                    'name'  => 'Thêm mới',
                    'icon'  => '',
                    'key'   => '',
                    'route' => '',
                ]
            ],
        ],
    ]

];
